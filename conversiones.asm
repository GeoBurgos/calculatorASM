;strbaseToInt(int base,int *inicioNum, int *finNum)
;EL inicio del numero viene DEC para comtemplar el primer digito
strbaseToInt:
    enter 0,0
    push edi
    mov esi,[ebp+16]        ;ESI = Fin del numero. Primer push
    mov ebx,[ebp+12]        ;EBX = Inicio del numero. Segundo push
    mov edi,[ebp+8]         ;EDI = Base del numero. Tercer push
    mov ecx,1               ;Iniciamos con la base elevada a 0 = 1
    sub eax,eax
    sub edx,edx
strciclo:
    cmp esi,ebx             ;If final del numero
    je strfin               ;Saltamos porque ya lo convertimos
    sub edx,edx
    mov dl,[esi]
    cmp dl,39h              ;Si el char en DL es mayor '9' entonces suponemos que es A,B,C,D,E,F
    jg strHexa              ;Entonces saltamos a hexadecimal
strDecimal:
    sub edx,30h
    jmp strSeguir
strHexa:
    sub edx,37h
strSeguir:
    dec esi
    cmp edi,edx             ;CMP debe el digito este dentro de los chars valido de la base
    jle strDigInv           ;Si no se imprimira el error
    imul edx,ecx            ;EDX = MUL el char por la base elevada a la posicion
    jo stroverflow
    imul ecx,edi            ;ECX = ECX*Base, esto para elevar la base segun la posicion
    jo stroverflow
    add eax,edx             ;EAX += EDX, es decir el EAX lleva el result
    jmp strciclo
stroverflow:
    mov ecx,03h
    jmp strfin
strDigInv:
    PutStr msgOverflow
    mov ecx,5
strfin:
    pop edi    
    leave
    ret

distBase:
    enter 0,0
    push edi
    mov esi,[ebp+8]
    mov edi,esi
distSigLetra:
    mov eax,[esi]
    inc esi
    inc edi
    mov ebx,[esi]
    cmp ebx,0
    je distEncontrada
    cmp bl,'('
    jge Inter1
Inter1:
    cmp bl,'/'
    jle distEncontrada
    jmp distSigLetra
distEncontrada:
    cmp al,30h
    jl distError
    cmp al,'b'
    je distBinario
    cmp al,'o'
    je distOctal
    cmp al,'d'
    je distDecimal
    cmp al,'h'
    je distHexadecimal
    jmp distDecEsp
distBinario:
    sub esi,2
    push esi
    mov esi,[ebp+8]
    dec esi
    mov ecx,2
    push esi
    push ecx
    call strbaseToInt
    pop ecx
    pop esi
    pop esi
    jmp distFin
distOctal:
    sub esi,2
    push esi
    mov esi,[ebp+8]
    dec esi
    mov ecx,8
    push esi
    push ecx
    call strbaseToInt
    pop ecx
    pop esi
    pop esi
    jmp distFin
distDecimal:
    sub esi,2
    push esi
    mov esi,[ebp+8]
    dec esi
    mov ecx,10
    push esi
    push ecx
    call strbaseToInt
    pop ecx
    pop esi
    pop esi
    jmp distFin
distDecEsp:
    dec esi
    push esi
    mov esi,[ebp+8]
    dec esi
    mov ecx,10
    push esi
    push ecx
    call strbaseToInt
    pop ecx
    pop esi
    pop esi    
    jmp distFin
distHexadecimal:
    sub esi,2
    push esi
    mov esi,[ebp+8]
    dec esi
    mov ecx,16
    push esi
    push ecx
    call strbaseToInt
    pop ecx
    pop esi
    pop esi
    jmp distFin
distError:
    PutStr analisisErrMsj
distFin:
    mov esi,edi
    pop edi
    leave
    ret
