validarExpresion:
    enter 0,0
    mov esi,[ebp+8]
    push esi
    call validarParentesis
    pop esi
    cmp edi,0
    jne valExpErr
    push esi
    call validarComb
    pop esi
    cmp edi,2
    je valExpErr    
    mov edi,expArreglada    
valExpSigChar:    
    mov eax,[esi]
    inc esi
    mov ebx,[esi]
    cmp eax,0
    je valExpFin
    cmp al,'-'
    je valExpResta
    cmp al,'+'
    je valExpSuma
    mov [edi],al
    inc edi
    jmp valExpSigChar
valExpResta:
    cmp bl,'-'
    je dobleMenos
    cmp bl,'+'
    je resSuma
    mov [edi],al
    inc edi
    jmp valExpSigChar
valExpSuma:
    cmp bl,'+'
    je dobleSuma
    cmp bl,'-'
    je resSuma
    mov [edi],al
    inc edi
    jmp valExpSigChar
dobleMenos:
    mov byte[edi],'+'
    inc edi
    inc esi
    jmp valExpSigChar
dobleSuma:
    mov byte[edi],'+'
    inc edi
    inc esi
    jmp valExpSigChar
resSuma:
    mov byte[edi],'-'
    inc edi
    inc esi
    jmp valExpSigChar
valExpErr:
    mov edi,1
valExpFin:
    leave
    ret
;----------------------------------------------------------------------------

validarParentesis:
    enter 0,0    
    mov esi,[ebp+8]
    sub edi,edi
valParSigChar:
    mov eax,[esi]
    cmp eax,0
    je valExpFin
    inc esi
    cmp al,'('
    je valParAbierto
    cmp al,')'
    je valParCerrado
    jmp valParSigChar
valParAbierto:
    inc edi
    jmp valParSigChar
valParCerrado:    
    dec edi
    jmp valParSigChar
valParFin:
    leave
    ret
;----------------------------------------------------------------------------

validarComb:
    enter 0,0
    mov esi,[ebp+8]    
valCombSigChar:
    sub edi,edi
    mov eax,[esi]
    inc esi
    mov ebx,[esi]
    cmp ebx,0
    je valCombFin
    cmp al,'-'
    je valCombResta
    cmp al,'+'
    je valCombSuma
    cmp al,'*'
    je valCombMul
    cmp al,'/'
    je valCombDiv
    cmp al,'('
    je validarAbierto
    jmp valCombSigChar
valCombResta:
    inc edi
    cmp edi,2
    je valCombFin
    cmp bl,'*'
    je valCombMul
    cmp bl,'/'
    je valCombDiv
    cmp bl,')'
    je validarCerrado
    jmp valCombSigChar
valCombSuma:
    inc edi
    cmp edi,2
    je valCombFin
    cmp bl,'*'
    je valCombMul
    cmp bl,'/'
    je valCombDiv
    cmp bl,')'
    je validarCerrado
    jmp valCombSigChar
valCombMul:
    inc edi
    cmp edi,2
    je valCombFin
    cmp bl,'/'
    je valCombDiv
    cmp bl,')'
    je validarCerrado
    jmp valCombSigChar
valCombDiv:
    inc edi
    cmp edi,2
    je valCombFin
    cmp bl,'*'
    je valCombMul
    cmp bl,')'
    je validarCerrado
    jmp valCombSigChar
validarAbierto:
    inc edi
    cmp edi,2
    je valCombFin
    cmp bl,'*'
    je valCombMul
    cmp bl,'/'
    je valCombDiv
    cmp bl,')'
    je validarCerrado
    jmp valCombSigChar
validarCerrado:
    inc edi
    cmp edi,2
    je valCombFin    
    jmp valCombSigChar
valCombFin:
    leave
    ret
