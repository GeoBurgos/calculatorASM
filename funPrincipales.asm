;--------------------------------------------------------------------------------------
size:
    enter 0,0
    mov edi,[ebp+8]             ;EDI = puntero de str[0] cargado en la pila
    sub ecx,ecx                 ;Limpiamos el registro EDX donde guardaremos el size
sigLetraSiz:
    mov eax,[edi]               ;EAX = caracter en la posicion de memoria de EDI
    inc ecx                     ;Incrementamos por cada caracter hasta el nulo
    inc edi                     ;EDI = sig. direccion de memoria de string
    cmp al,0                    ;Comparar hasta encontrar un nulo
    je finSize                  ;Si se cumple ya  tenemos el size de la cadena y salimos del proc
    jmp sigLetraSiz             ;Si no seguimos hasta encontrar un nulo
finSize:
    leave
    ret
