comandosProc:
    mov esi,expresion           ;ESI = dir. del la expresion ingresada por el usuario
    mov edi,comandosArray       ;EDI = dir. del la lista de comandos especiales
    sub ebx,ebx                 ;Limpiamos registro donde guardaremos indice del comando
sigLetraCom:
    push edi                    ;Guardamos la dir. del comando a comparar con la expresion
    call size                   ;Nos calcula el size de una cadena
    pop edi                     ;Reestablecemos el valor para comparar hora sí
    mov eax,ecx                 ;Movemos al EAX el size de la cadena a comparar del array
    push esi                    ;Guardar ESI porque el cmps la cambia
    push edi                    ;Guardar EDI porque el cmps la cambia
    inc ebx                     ;Inc el EBX para indicar cual cadena estamos en el array
    repe cmpsb                  ;Repetimos la comparacion para el size de la cadena en ECX
    pop edi                     ;Restauramos el EDI
    pop esi                     ;Restauramos el ESI
    je indiceCom                ;Si se cumple la comparacion pasamos a lo que hace el comando
    cmp ebx,6                   ;Si el indice ya es 6 es que ya terminamos la lista
    je indiceCom                ;Y saltamos a terminar el procedimiento
    add edi,eax                 ;Add para comparar con el sig elemento del array
    jne sigLetraCom             ;Si no fuera igual entonces analizamos el sig. elemento del array
indiceCom:
    cmp bl,4                    ;Si la cadena es igual a la cadena del array[4]
    je salirCom                 ;Salta a la etiqueta para salir por completo del programa    
    cmp bl,1                    ;Si la cadena es igual a la cadena del array[1]
    je procCom                  ;Salta a la etiqueta para solicitar mostrar procedimiento
    cmp bl,2                    ;Si la cadena es igual a la cadena del array[2]
    je bitsCom                  ;Salta a la etiqueta para solicitar el # de bits de presicion
    cmp bl,3                    ;Si la cadena es igual a la cadena del array[3]
    je varCom                   ;Salta a la etiqueta para mostrar las variables existentes del programa
    cmp bl,5                    ;Si la cadena es igual a la cadena del array[0]
    je ayudaCom                 ;Salta a la etiqueta para mostrar todas las ayudas
    jmp ayudaCom1               ;Salta a la etiqueta para mostrar ayuda especializada
ayudaCom:
    call ayuda
    jmp finCom
ayudaCom1:
    call ayudaEsp
    jmp finCom
procCom:
    call procedimiento
    jmp finCom
bitsCom:
    call bitz
    jmp finCom
varCom:
    PutStr varMsj
    jmp finCom
salirCom:
    jmp terminar
finCom:
    ret
