;sudo dpkg-reconfigure keyboard-configuration
;Postfijo hechas:
;   2*(1+1) = 2 1 1 + *
;   (2+(3*4)) = 2 3 4 * +
;   (1+(2/(7-3)+4)*1)-10 = 1 2 7 3 - / 4 + 1 * + 10 -
;   2/(10/(514*7+1))+(25-5*4)-10+20 = 2 10 514 7 * 1 + / /25 5 4 * - + 10 - 20 +    Ya 
;Calculadora entre bases numericas              CALCULATOR.ASM
;        Objective: 
;                   Imprimir el  resultado en de una expresion
;                   ingresada por el usuario especificando las
;                   bases numericas de cada operando como tam-
;                   bien en base se debe mostrar el resultado.
;            Input: Requiere una expresion del usuarrio.
;           Output: Imprimir resultado de la expresion mostrando
;                   o no el procedimiento
%include "io.mac"
%include "distrComandos.asm"
%include "comandos.asm"
%include "funPrincipales.asm"
%include "complemento.asm"
%include "conversiones.asm"
%include "declaraciones.asm"
%include "pilaOperadores.asm"
%include "pilaOperandos.asm"
%include "resolucionExp.asm"
%include "validaciones.asm"
%include "limpieza.asm"
%include "solucion.asm"
.DATA
interprete      db  '>> ',0
comandosArray   db  '#procedimiento',0,'#bits',0,'#var',0,'#salir',0,'#ayuda',0 ;34
ayudaMsj        db  'Comando especiales: #ayuda, #procedimiento, #bits, #var, #salir',13,10,0
procMsj         db  'Desea visualizar procedimiento?',13,10,'Ingrese 1 para SI o 0 para NO): ',0
bitsMsj         db  'Desea definir los bits de presicion?',13,10,'Ingrese alguno de los siguientes numeros segun la presicion que desee 4,8,16,32: ',0
varMsj          db  'name1 = 0',0
salirMsj        db  'Estudiantes: Leonardo Mata/Geovanny Burgos',13,10
                db  'Universidad: Tecnologico de Costa Rica',13,10
                db  'Carrera: Ingenieria en Computacion',13,10
                db  'Curso: IC3101-Arquitectura de computadoras',13,10
                db  'Profesor: Esteban Arias Mendez',13,10
                db  'Periodo: I Semestre. Año: 2017',13,10,0
viewProc        db  0
bitsPrec        db  4
procErrMsj      db  'Valor incorrecto. Debe ser 1 o 0',0
bitzErrMsj      db  'Valor incorrecto. Debe ser 4,8,16 o 32',0
ayudaMul        db  13,10,'Ayuda multiplicacion',13,10
                db  '   Se debe de ingresar dos numeros a,b',13,10
                db  '   b debe de ser cualquier numero al igual que b, mientras que ambos sean enteros',13,10
                db  '   Para indicar la multiplicacion se debe de usar el simbolo * ',13,10
                db  '   Se pueden multiplicar numeros positivos y negativos',13,10,0
ayudaDiv        db  13,10,'Ayuda division',13,10
                db  '   Se debe de ingresar dos numeros a,b',13,10
                db  '   b debe de ser diferente de 0 y a puede ser cualquier numero entero',13,10
                db  '   Para indicar la division se debe de usar el simbolo / ',13,10,0
                
ayudaSum        db  13,10,'Ayuda suma',13,10
                db  '   Se debe de ingresar dos numeros a,b',13,10
                db  '   b debe de ser cualquier numero al igual que b, mientras que ambos sean enteros',13,10
                db  '   Para indicar la suma se debe de usar el simbolo + ',13,10
                db  '   Se pueden sumar numeros positivos y negativos',13,10,0
ayudaRes        db  13,10,'Ayuda resta',13,10
                db  '   Se debe de ingresar dos numeros a,b',13,10
                db  '   b debe de ser cualquier numero al igual que b, mientras que ambos sean enteros',13,10
                db  '   Para indicar la resta se debe de usar el simbolo - ',13,10
                db  '   Se pueden restar numeros positivos y negativos',13,10,0
analisisErrMsj  db  'Simbolos incorrectos en la expresion',0
soliAyudas      db  '1. Ayuda Multiplicacion',13,10
                db  '2. Ayuda Division',13,10
                db  '3. Ayuda Suma',13,10
                db  '4. Ayuda Resta',13,10
                db  '5. Todas las ayudas',13,10
                db  'Ingrese el numero de la opción que desea: ',0
opcionInvalida  db  'Ingresaste una opcion invalida',0
mostrarCompl    db  'Complemento: ',13,10,0
tabs            db  '   ',0
line            db  '¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯',0
MAX_SIZE        EQU 512
msgErrResolver  db  'Error: Expresion invalida',0
PDP             db  0,1,5
msgOverflow     db  'Error: Desbordamiento',13,10,0
msgExpPostfija  db  'La expresion postfija es: ',0
msg_Repetido    db  'Ya se encuentra registrada la variable. ',13,10,0

sOperando       db  'Salio de operando',13,10,0
sOperador       db  'Salio de operador',13,10,0
eOperando       db  'Entro a operando',13,10,0
eOperador       db  'Entro a operador',13,10,0
pilavacia       db  'Pila vacia',13,10,0
sacarhasta      db  'Sacar hasta (',13,10,0
sacarSum        db  'Entro a pop suma',13,10,0
sacarMul        db  'Entro a pop resta',13,10,0
msgcamino       db  'Entro ',13,10,0
msgcamino1       db  'Entro a solucion ',13,10,0
msg_Cero        db  'Division entre cero ',13,10,0
msgResulPost    db  'Resultado de la expresion: ',0
varValor        db  'El valor de la varaible guardada es: ',0

.UDATA
expresion       resb 64
expArreglada    resb 64
expPost         resq 64
operando        resd 1
array           resb 512
arrayValor      resq 32
variable        resb 16
pilaOperandos   resq 20     ;Campo para 20 numeros de 4 bytes cada uno(Claro ya convertidos)
pilaOperadores  resb 20     ;Campo para almacenar 20 operadores que cada uno es de un byte
topeOperandos   resq 1      ;Aqui esta la dir. de memoria del tope de la pila de operandos
topeOperadores  resq 1      ;Aqui esta la dir. de memoria del tope de la pila de operadores
charAnt         resb 1

.CODE
    .STARTUP
; ; ;     sub eax,eax
; ; ;     sub ebx,ebx
; ; ;     sub edx,edx
; ; ;     mov eax,250
; ; ;     mov ebx,-100
; ; ; ;     cmp eax,0
; ; ; ;     jl negativo1
; ; ; ;     cmp ebx,0
; ; ; ;     jl negativo1
; ; ; ; negativo1:
; ; ;     neg ebx
; ; ;     mov edx,0
; ; ;     idiv dword ebx
; ; ;     neg eax       
; ; ;     PutLInt eax
;     lea esi,[pilaOperandos]
;     mov edi,topeOperandos    
;     mov [edi],esi
;     cmp [edi],esi
;     je vacia
    lea esi,[pilaOperandos]
    mov edi,topeOperandos
    mov dword[edi],esi    
    lea esi,[pilaOperadores]
    mov edi,topeOperadores
    mov dword[edi],esi
    sub esi,esi
    sub edi,edi
;     mov eax,1
;     push eax
;     call pushOperador
;     pop eax
;     mov eax,4
;     push eax
;     call pushOperador
;     pop eax
;     call popOperador
;     PutLInt eax
;     nwln
;     call popOperador
;     PutLInt eax
; vacia:
;     nwln
;     nwln
;     nwln
;     jmp terminar
newExp:
    call limpiarExp
    call limpiarExpArreglada
    call limpiarExpPost
    nwln
    PutStr interprete           ;Imprimir >>
    GetStr expresion,50         ;Obtener y guardar la expresion
    cmp byte[expresion],23h     ;Comparar entrada con simbolo #
    je comandos                 ;Si cmp es igual salta para entrar a los comandos
    cmp byte[expresion],7Eh     ;Comparar entrada con simbolo ~
    je complemento              ;Si cmp es igual salta para hacer complemento
    cmp byte[expresion],1Bh     ;Comparar entrada con simbolo esc
    je terminar                 ;Sí cmp es igual salte para terminar programa
    jmp analizarExp
comandos:
    pusha
    call comandosProc
    popa
    jmp newExp
complemento:
    pusha
    call complementoProc
    popa
    jmp newExp
analizarExp:
    pusha
    call analizarExpProc
    popa
    jmp newExp
terminar:
    PutStr salirMsj
    .EXIT
;----------------------------------------------------------------------------

analizarExpProc:
    mov esi,expresion
analizarSigChar:
    mov eax,[esi]   
    inc esi
;     cmp al,1Fh
;     jle anError
    cmp al,'{'
    jge anError
    cmp al,'.'
    je anPunto
    cmp al,':'
    je andeclaracion
    cmp al,0
    je anExpresion
    jmp analizarSigChar
anExpresion:
    call crearPostfija
    call printPosfija
    call solucionarPostfija
    sub eax,eax
    call popOperando
    nwln
    PutStr msgResulPost
    PutLInt eax
    jmp anFin
andeclaracion:
    call declaracion
    jmp anFin
anPunto:
    call puntoFlotante
    jmp anFin
anError:
    PutStr analisisErrMsj
anFin:    
    ret
;----------------------------------------------------------------------------

puntoFlotante:
    
    ret
;----------------------------------------------------------------------------

declaracion:
    call validacion
    ret
