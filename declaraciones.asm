validacion:
    
    sub esi,esi
    sub ecx,ecx
    call contador
    cmp ecx,0
    je declaration
    sub esi,esi
    mov edi,expresion
    jmp validar

validar1:
    inc esi
    mov edi,expresion

validar:
    mov ebx,[array+ESI*2]
    cmp bl,0h
    je declaration
    mov eax,[edi]
    cmp bl,al
    je auxValidar
    jmp auxValidar2

auxValidar:
    inc esi 
    inc edi
     mov ebx,[array+ESI*2]
    mov eax, [edi]
    jmp validar5

auxValidar2:

    cmp bl,24h
    je validar1  
    jmp auxValidar3
    
auxValidar3:
    inc esi
    mov ebx,[array+ESI*2]
    jmp auxValidar2

validar5:
    cmp al,3ah
    jne validar;                var:                var1
    
validar4:
    cmp bl,24h
    jne validar
;     PutStr msg_Repetido
    ret

declaration:
    enter 0,0
    ;sub ESI,ESI     ; se limpian los registros
    mov CX,MAX_SIZE    
    call contador     ; se llama al procedimiento contador para saber la longitud de la lista que tiene las variables
    sub esi,esi 
   ; PutLInt ecx
    add esi,ecx         ;se le suma al esi el largo de la lista para que guarde las variables en orden
;     jmp principal          ;salto al procedimiento principal        
principal:
;     GetStr variable            ;
    mov edi,expresion        ;movemos la direccion de la variable al registro di
aux:
    mov eax,[edi]           
;      PutCh al
; ;     nwln
    cmp Al, 3ah             ; comparamos el elemento para saber si es igual al dos puntos
    je aux2        
    mov [array+ESI*2],AX    ;si no es igual metemos el caracter en la lista
    inc ESI          
    inc edi
    jmp aux
aux2:
    mov eax,24h
    mov [array+ESI*2],eax  ; se mueve a la lista el simbolo $ para luego saber que el nombre de la variable termino
    sub ESI,ESI          ;limpiamos registros
    inc edi               ;limpiamos registros
    sub esi,esi              ;limpiamos registros
    sub ebx,ebx             ;limpiamos registros
    sub ecx,ecx
    call contador
   ; PutLInt ecx
    sub esi,esi 
   ; PutLInt esi
    jmp verLista    
      
verLista:

    mov ebx,[array+ESI*2]      ;se mueve el primer valor de la lista al registro ebx
    cmp bl,0h                  ;se compara con 0 para saber si ya se llego al final
    je fin
;     PutCh bl
    inc ESI
    loop verLista     
fin:
    jmp arrayValores
    leave
    ret

contador:
    mov ebx,[array+ESI*2]   ;pasa el primer valor de la lista al ebx
    cmp bl,0h                ;compara el valor para ver si esta en null se termina el procedimiento
    je contadorfin
    ; PutCh bl
    add edx,1               ;se suma 1 al registro edx para saber la longitud de la lista
    inc esi                 ;se incrementa el esi
    jmp contador            ; salto incondicional al procedimiento contador 
contadorfin:
    mov cx,dx              ; mover el valor del registro dx el cual contenia el numero de veces que se estaba repitiendo el procedimiento
   
    ;PutLInt ecx    
   ; inc cx
    ret 

    
    
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

; declarationValores:
;     enter 0,0
;     sub ESI,ESI     ; se limpian los registros
;     mov CX,MAX_SIZE    
;     call contadorValores    ; se llama al procedimiento contador para saber la longitud de la lista que tiene las variables
;     sub esi,esi 
;     PutLInt ecx
;     add esi,ecx         ;se le suma al esi el largo de la lista para que guarde las variables en orden
; ;     jmp principal          ;salto al procedimiento principal        
; principal:
; ;     GetStr variable            ;
;     mov edi,expresion        ;movemos la direccion de la variable al registro di
;   
arrayValores:
    mov esi,edi
    call crearPostfijaAux
    call solucionarPostfija
    sub eax,eax
    call popOperando
; ;     nwln
  ;  PutLInt eax
     mov ebx,eax
     mov edi,eax
    push eax
    call pushOperando
    pop eax
; ;     nwln
 arrayAux:  
    mov eax,edi           ;se mueve el caracter al registro eax
    ;PutStr msg_Repetido                        ;salta al procedimiento finVariables
    
   ; PutLInt eax
; ;     nwln
    sub edx,edx
    sub ecx,ecx
     sub esi,esi         
    sub ebx,ebx
    call contadorValores     
    ;PutLInt ecx
    sub ebx,ebx
    sub esi,esi
    add esi,ecx
    push eax
    ;PutLInt eax
    mov [arrayValor+ESI*2],AX 
    pop eax
   ;PutLInt eax
   inc ESI          
    inc edi

finValores:
    mov eax,24h
    mov [arrayValor+ESI*2],AX 
    sub esi,esi         
    sub ebx,ebx
    call contadorValores     
   ; PutLInt ecx
; ;     nwln
    sub ebx,ebx
    sub esi,esi
    ;PutLInt esi
    jmp verListaValores        
verListaValores:
    sub eax,eax
 
    mov ax,[arrayValor+ESI*2]      ;se mueve el primer valor de la lista al registro ebx

    cmp al,0h                  ;se compara con 0 para saber si ya se llego al final
    je finValores1
; ;     nwln
    cmp eax,24h
    je siguiente
    PutLInt eax
    inc ESI
    jmp verListaValores     

finValores1:
   
    leave
    ret
siguiente:
    inc esi
    jmp verListaValores
contadorValores:
    mov ebx,[arrayValor+ESI*2]   ;pasa el primer valor de la lista al ebx
    cmp bl,0h                ;compara el valor para ver si esta en null se termina el procedimiento
    je contadorfinValores
    ; PutCh bl
    add edx,1               ;se suma 1 al registro edx para saber la longitud de la lista
    inc esi                 ;se incrementa el esi
    jmp contadorValores            ; salto incondicional al procedimiento contador 
contadorfinValores:
    mov cx,dx              ; mover el valor del registro dx el cual contenia el numero de veces que se estaba repitiendo el procedimiento
;     PutStr varValor
;     PutLInt ecx
;     inc cx
    ret 

    
    
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;



