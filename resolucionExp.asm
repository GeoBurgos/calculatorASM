crearPostfijaAux:

   ; mov esi,expresion
    push esi
    call validarExpresion
    pop esi
    cmp edi,1
    je resErr    
    mov esi,expArreglada
    mov eax,[esi]
    cmp al,'*'
    je resErr
    cmp al,'/'
    je resErr
    cmp al,')'
    je resErr
    cmp al,'.'
    je resErr
    cmp al,','
    je resErr
    mov esi,expArreglada
    mov edi,expPost
    jmp resSigChar
;     PutCh 'e'
;     PutCh 'd'
;     PutCh 'i'
;     PutLInt edi
;     nwln
;     PutLInt expPost
crearPostfija:
    mov esi,expresion
    push esi
    call validarExpresion
    pop esi
    cmp edi,1
    je resErr    
    mov esi,expArreglada
    mov eax,[esi]
    cmp al,'*'
    je resErr
    cmp al,'/'
    je resErr
    cmp al,')'
    je resErr
    cmp al,'.'
    je resErr
    cmp al,','
    je resErr
    mov esi,expArreglada
    mov edi,expPost
;     PutCh 'e'
;     PutCh 'd'
;     PutCh 'i'
;     PutLInt edi
;     nwln
;     PutLInt expPost
resSigChar:
; ; ; ;     PutCh 'e'
; ; ; ;     PutCh 'd'
; ; ; ;     PutCh 'i'
; ; ; ;     PutLInt edi
; ; ; ;     nwln
    mov eax,[esi]
    mov ebx,[esi+1]
    cmp eax,0
    je resolverFin
    cmp al,' '
    je resSigChar
    cmp al,'&'
    je resErr   ; Aqui debia ir lo de la variable
    cmp al,27h ;'''
    jle resErr
    cmp al,'.'
    je resErr
    cmp al,','
    je resErr
    cmp al,'('
    jge resOperadorInter
resOperadorInter:
    cmp al,'/'
    jle resOperador
    cmp al,'9'
    jle resOperando
    cmp al,'='
    je resBase
    cmp al,'@'
    jle resErr
    cmp al,'F'
    jle resOperando
    jmp resErr
resVar:
;     push edi
;     call expVariable    ;Necesito la lista bien hecha
;     pop edi             ;Recordar retornar el valor de la variable en eax sino cambiar en la pila
;     mov [edi],eax       ;que al pop del edi retorne un 1 si hubo error
;     add edi,4
    jmp resSigChar
resOperador:
; ; ; ;     PutStr eOperador
    sub eax,eax
    mov al,[esi]
;     push edi
    push esi
    call expOperador    ;Necesito comprender lo de la pila
    pop esi
;     pop edi
    inc esi
; ; ; ;     PutStr sOperador
    jmp resSigChar
resOperando:
; ; ; ;     PutStr eOperando
    push esi
    call distBase
;     PutLInt eax
    mov [edi],eax
;     PutLInt [edi]
    add edi,4
    pop eax
;     PutLInt [expPost]
;     nwln
; ; ; ;     PutStr sOperando
    jmp resSigChar
resBase:
;     call expPrintBase
    jmp resolverFin
resErr:
    PutStr msgErrResolver
resolverFin:
    call vaciarOperadores
    PutStr msgExpPostfija
;     call printPosfija
    ret
;--------------------------------------------------------------------------;
;--------------------------------------------------------------------------;
;--------------------------------------------------------------------------;
expVariable:
    enter 0,0
    
    leave
    ret

;--------------------------------------------------------------------------;
;--------------------------------------------------------------------------;
;--------------------------------------------------------------------------;
printPosfija:
    enter 0,0
    mov esi,expPost
sigElemento:
    mov eax,dword[esi]
;     PutCh al
;     nwln
    PutCh ' ' 
    add esi,4
    cmp eax,0
    je verificarfin
    jmp printPosSeguir
verificarfin:
    mov ebx,dword[esi]
    cmp ebx,0
    je printFin
printPosSeguir:
    cmp al, '*'
    je printInt
    cmp al,'/'
    je printInt
    cmp al,'+'
    je printInt
    cmp al,'-'
    je printInt
    PutLInt eax
    jmp sigElemento
printInt:
    PutCh al
    jmp sigElemento
printFin:
    leave
    ret
    
