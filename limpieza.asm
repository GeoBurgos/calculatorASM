limpiarExpArreglada:
    enter 0,0
    mov esi,expArreglada
    mov ecx,64
limpiarExpA:
    mov byte[esi],0
    inc esi
    loop limpiarExpA
    leave
    ret
    
limpiarExpPost:
    enter 0,0
    mov esi,expPost
    mov ecx,64
limpiarExpP:
    mov dword[esi],0
    add esi,4
    loop limpiarExpP
    leave
    ret

limpiarExp:
    enter 0,0
    mov esi,expresion
    mov ecx,64
limpiarE:
    mov byte[esi],0
    inc esi
    loop limpiarE
    leave
    ret
