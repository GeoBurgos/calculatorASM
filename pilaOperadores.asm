pushOperador:
    enter 0,0
    push edi
    mov eax,[ebp+8]
;     PutCh al
;     nwln
    mov esi,[topeOperadores]
;     PutLInt esi
;     nwln
    mov edi,topeOperadores
    mov [esi],eax
    inc esi
    mov [edi],esi
;     PutLInt [edi]
;     nwln
;     PutLInt pilaOperadores
;     nwln
    pop edi
    leave
    ret

popOperador:
    enter 0,0
    push edi
    mov esi,[topeOperadores]
    mov edi,topeOperadores
    mov byte[esi],0
    dec esi
    mov eax,[esi]
    mov [edi],esi    
    pop edi
    leave
    ret

vaciarOperadores:
    enter 0,0
pilaVaciar:
    lea esi,[pilaOperadores]
    mov ebx,topeOperadores
    cmp [ebx],esi
    je pilaVaciada
    call popOperador
    mov [edi],eax
    add edi,4
    jmp pilaVaciar
pilaVaciada:    
    leave
    ret

;--------------------------------------------------------------------------;
;--------------------------------------------------------------------------;
;--------------------------------------------------------------------------;
expOperador:
    enter 0,0
    mov ecx,eax
    lea esi,[pilaOperadores]
    mov ebx,topeOperadores
    cmp [ebx],esi
    je expPilaVacia
    cmp cl,')'  
    je expPopsParentesis
    cmp cl,'*'
    je mulDivFP
    cmp cl,'/'
    je mulDivFP
    cmp cl,'+'
    je sumResFP
    cmp cl,'-'
    je sumResFP
    cmp cl,'('
    je expPush
expPilaVacia:
; ; ; ;     PutStr pilavacia
    push ecx    
    call pushOperador
    pop ecx
    jmp expOperadorFin
expPopsParentesis:
; ; ; ;     PutStr sacarhasta
    call popOperador
    cmp al,'('
    je expOperadorFin
    mov [edi],eax
;     PutStr edi
;     nwln
    add edi,4
    jmp expPopsParentesis
mulDivFP:
    mov al,cl
    lea esi,[pilaOperadores]
    mov ebx,topeOperadores
    cmp [ebx],esi
    je expPilaVacia
    mov ebx,[topeOperadores]
    dec ebx
    mov edx,[ebx]
; ; ; ;     PutCh al
; ; ; ;     PutCh '?'
; ; ; ;     PutCh dl
; ; ; ;     nwln
    cmp dl,'/'
    je popMulDiv
    cmp dl,'*'
    je popMulDiv
    jmp expPush
popMulDiv:
; ; ; ;     PutStr sacarMul
; ; ; ;     PutLInt [topeOperadores]
    call popOperador
; ; ; ;     nwln
; ; ; ;     PutLInt [topeOperadores]
    mov [edi],eax
;     PutStr edi
; ; ; ;     nwln
    add edi,4
    jmp mulDivFP
sumResFP:
    mov al,cl
    lea esi,[pilaOperadores]
    mov ebx,topeOperadores
    cmp [ebx],esi
    je expPilaVacia
    mov ebx,[topeOperadores]
    dec ebx
    mov edx,[ebx]
; ; ; ;     PutCh al
; ; ; ;     PutCh '?'
; ; ; ;     PutCh dl
; ; ; ;     nwln
; ; ; ;     PutCh 'H'
; ; ; ;     nwln
    cmp dl,'/'
    je popSumRes
    cmp dl,'*'
    je popSumRes
    cmp dl,'+'
    je popSumRes
    cmp dl,'-'
    je popSumRes
    jmp expPush
popSumRes:
; ; ; ;     PutStr sacarSum
    call popOperador
; ; ; ;     PutCh al
; ; ; ;     nwln
    mov [edi],eax
; ; ; ;     PutStr edi
; ; ; ;     nwln
    add edi,4
    jmp sumResFP
; parentesisFP:
;     mov al,cl
;     lea esi,[pilaOperadores]
;     mov ebx,topeOperadores
;     cmp [ebx],esi
;     je expPilaVacia
;     mov ebx,[topeOperadores]
;     dec ebx
;     mov edx,[ebx]
;     PutCh al
;     PutCh '?'
;     PutCh dl
;     nwln
;     cmp dl,'/'
;     je popParentesis
;     cmp dl,'*'
;     je popParentesis
;     cmp dl,'+'
;     je popParentesis
;     cmp dl,'-'
;     je popParentesis
;     cmp dl,'('
;     je popParentesis
;     jmp expPush
; popParentesis:
;     call popOperador
; ;     PutCh al
; ;     nwln
;     mov [edi],eax
; ;     PutStr edi
; ;     nwln
;     add edi,4
;     jmp parentesisFP
expPush:
    mov al,cl
; ; ; ;     PutCh 'P'
; ; ; ;     nwln
    push eax
    call pushOperador
    pop eax
    jmp expOperadorFin
expOperadorFin:
;     PutCh 'F'
    leave
    ret
