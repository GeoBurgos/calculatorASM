solucionarPostfija:
    enter 0,0
    sub esi,esi
    mov esi, expPost

auxSolucion1:
    sub eax, eax
    mov eax,dword[esi]
    add esi,4
;     nwln
;     PutLInt eax
;     PutCh al
;     nwln
    cmp eax,0
    je soluVerificarFin
    jmp seguirSolucion
soluVerificarFin:
    mov ebx,dword[esi]
    cmp ebx,0
    je finSolucion
seguirSolucion:
    cmp al, '*'
    je operacionMulti
    cmp al,'/'
    je  operacionDivision
    cmp al,'+'
    je operacionSuma
    cmp al,'-'
    je operacionResta
    jmp insertarOperando
   

insertarOperando:
    push esi
    push eax
    sub esi,esi
    call pushOperando
    pop eax
    pop esi
    sub eax,eax
    sub ebx,ebx
    jmp auxSolucion1
    
    
operacionMulti:
;     PutStr msgcamino1
    sub eax,eax
    push esi
    sub ebx,ebx
    call popOperando
    mov ebx,eax
    sub esi,esi
    sub eax,eax
    call popOperando
 
    imul eax,ebx
;     mul bl
   

    sub esi,esi
    push eax
    call pushOperando
    pop eax
    sub esi,esi
    pop esi
    jmp auxSolucion1
operacionSuma:
;     PutStr msgcamino1
    sub eax,eax
    push esi
    sub ebx,ebx
    call popOperando
    mov ebx,eax
    sub esi,esi
    sub eax,eax
    call popOperando
;        PutLInt eax
    add  eax,ebx
      
;     PutLInt eax
    sub esi,esi
    push eax
    call pushOperando
    pop eax
    sub esi,esi
    pop esi

    jmp auxSolucion1

operacionResta:
;     PutStr msgcamino1
    sub eax,eax
    push esi
    sub ebx,ebx
    call popOperando
    mov ebx,eax
    sub esi,esi
    sub eax,eax
    call popOperando
;     neg ebx
;     add eax,ebx
    sub eax,ebx
;     neg eax
   
;         PutStr msgcamino
;     PutLInt eax
    sub esi,esi
    push eax
    call pushOperando
    pop eax
    sub esi,esi
    pop esi
    jmp auxSolucion1

operacionDivision:
;     PutStr msgcamino1
    sub eax,eax
    push esi
    sub ebx,ebx
    call popOperando
    mov ebx,eax
    sub esi,esi
    sub eax,eax
    call popOperando
;     nwln
;     PutLInt eax
;     PutLInt ebx
;     nwln
    cmp ebx,0
    je divisionCero
    sub ecx,ecx
    cmp eax,0
    jl num1Negativo
    cmp ebx,0
    jl num2Negativo
    jmp seguirDivision
num1Negativo:
    inc ecx
    neg eax
    cmp ebx,0
    jl num2Negativo
    jmp seguirDivision
num2Negativo:
    inc ecx
    neg ebx
seguirDivision:
    mov edx,0
    idiv dword ebx
    cmp ecx,1
    jne divisionFin
    neg eax
divisionFin:
    sub esi,esi
    push eax
    call pushOperando
    pop eax
    sub esi,esi
    pop esi    
    jmp auxSolucion1
divisionCero:
    nwln
    PutStr msg_Cero
    jmp finSolucion

finSolucion:
    sub esi,esi
    push esi
    call popOperando
;     PutLInt eax
    push eax
    call pushOperando
    pop eax
    leave 
    ret
