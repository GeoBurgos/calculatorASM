;--------------------------------------------------------------------------------------
complementoProc:
    mov esi,expresion
    inc esi                 ;INC para no analizar el ~
    sub ecx,ecx
    mov eax,[esi]           ;Analizamos si bien un + o -
    cmp al,2Dh              ;IF AL == -
    je complNegativo        ;Saltara a realizar un complemento negativo
    cmp al,2Bh              ;ELSE If AL == +
    jne complMostrar        ;Saltara si no es un mas directo a realizar complemento
complPositivo:
    inc esi                 ;Si tenia un + entonces INC para evitarlo
    jmp complMostrar
complNegativo:
    inc ecx                 ;ECX = al numero de complementos a realizar al numero
    inc esi                 ;Si tenia un - entonces INC para evitarlo
complMostrar:
    inc ecx
    push ecx
    push esi
    call distBase
    pop esi
    pop ecx
complCiclo:
    push ecx
    push eax
    call hexToBin           ;Imprimimos cifra en binario
    PutStr tabs
    PutStr line
    nwln
    pop eax
    not eax                 ;Invertimos los bits
    push eax
    call hexToBin           ;Imprimimos el numero con los bits invertido
    pop eax
    mov ebx,1
    push ebx
    call hexToBin           ;Imprimimos el 1 al que le sumamos el numero invertido
    PutStr tabs
    PutStr line
    nwln
    pop ebx
    inc eax                 ;Hacemos la suma al numero que le invertimos los bits
    push eax
    call hexToBin           ;Imprimimos el numero original ya invertido
    pop eax
    pop ecx
    loop complCiclo
complFin:    
    ret

;Esta funcion recibe los parametros por paso de pila entonces antes de llamarla se debe 
;hacer push del numero a imprimir en binario. En el EAX manejamos el numero pasado por
;la pila que es el parametro. Luego realizamos un ciclo que en cada uno revisa si el 
;acarreo, segun el digito binario asi imprimira en pantalla
hexToBin:
    enter 0,0
    push eax
    PutStr tabs
    mov eax,[ebp+8]
    sub ecx, ecx
    mov ecx,20h
bit:
    rcl eax,1
    jc print_0
    PutCh '0'
    jmp skip1
print_0:
    PutCh '1'
skip1:
    loop bit
    nwln
    pop eax
    leave
    ret
