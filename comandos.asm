;--------------------------------------------------------------------------------------
ayuda:
    PutStr soliAyudas
    GetInt ax
    cmp al,1
    je ayudaMulti
    cmp al,2
    je ayudaDivi
    cmp al,3
    je ayudaSuma
    cmp al,4
    je ayudaResta
    cmp al,5
    je ayudaTodas
    jne ayudaError
ayudaMulti:
    PutStr ayudaMul
    jmp ayudaFin
ayudaDivi:
    PutStr ayudaDiv
    jmp ayudaFin
ayudaSuma:
    PutStr ayudaSum
    jmp ayudaFin
ayudaResta:
    PutStr ayudaRes
    jmp ayudaFin
ayudaError:
    PutStr opcionInvalida
    jmp ayudaFin
ayudaTodas:
    PutStr ayudaMul
    PutStr ayudaDiv
    PutStr ayudaSum
    PutStr ayudaRes
ayudaFin:
    ret

;--------------------------------------------------------------------------------------
ayudaEsp:

    ret
;--------------------------------------------------------------------------------------
procedimiento:    
    PutStr procMsj
    GetInt ax
    cmp al,1
    je procYes
    cmp al,0
    jne procError
    mov [viewProc],al
    jmp procFin
procYes:
    mov [viewProc],al    
    jmp procFin
procError:
    PutStr procErrMsj    
procFin:
    ret

;--------------------------------------------------------------------------------------
bitz:
    PutStr bitsMsj
    GetInt ax
    cmp al,4
    je bitzSave
    cmp al,8
    je bitzSave
    cmp al,16
    je bitzSave
    cmp al,32
    je bitzSave
    jne bitzError
bitzSave:
    mov [bitsPrec],al
    jmp bitzFin
bitzError:
    PutStr bitzErrMsj    
bitzFin:
    ret
