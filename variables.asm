;
;
%include "io.mac"

.DATA
MAX_SIZE       EQU 100
input_prompt   db  "Ingrese la variable: "



.UDATA
array          resw  MAX_SIZE
arrayValor     resw  MAX_SIZE
variable        resd 16
.CODE
        .STARTUP

        
inicio:
        PutStr  input_prompt
        nwln
        sub     ESI,ESI     ; se limpian los registros
        mov     CX,MAX_SIZE    
        call   contador     ; se llama al procedimiento contador para saber la longitud de la lista que tiene las variables
        PutLInt ecx
        add    esi,ecx         ;se le suma al esi el largo de la lista para que guarde las variables en orden
        jmp principal          ;salto al procedimiento principal

 contador:
         mov ebx,[array+ESI*2]   ;pasa el primer valor de la lista al ebx
        cmp bl,0h                ;compara el valor para ver si esta en null se termina el procedimiento
        je contadorfin
       ; PutCh bl
        add edx,1               ;se suma 1 al registro edx para saber la longitud de la lista
        inc esi                 ;se incrementa el esi
        jmp contador            ; salto incondicional al procedimiento contador 

contadorfin:
        mov cx,dx              ; mover el valor del registro dx el cual contenia el numero de veces que se estaba repitiendo el procedimiento
        ;PutLInt ecx
        ret
        
principal:
        GetStr  variable            ; 
        mov     edi,variable        ;movemos la direccion de la variable al registro di
aux:
        mov     eax,[edi]           
       ; PutCh al
        nwln
        cmp     Al, 3ah             ; comparamos el elemento para saber si es igual al :
        je      aux2        
        mov     [array+ESI*2],AX    ;si no es igual metemos el caracter en la lista
        inc     ESI          
        inc     edi
        jmp aux
    

aux2:
    mov eax,24h
    mov  [array+ESI*2],eax  ; se mueve a la lista el simbolo $ para luego saber que el nombre de la variable termino
    sub     ESI,ESI          ;limpiamos registros
    inc     edi               ;limpiamos registros
    sub esi,esi              ;limpiamos registros
    sub ebx,ebx             ;limpiamos registros
    jmp arrayValores
    
    
arrayValores:
    mov eax,[edi]           ;se mueve el caracter al registro eax
    cmp al,0h               ;se compara con 0 para saber si ya se llego al final
    je finVariables         ;salta al procedimiento finVariables
    PutCh al
    nwln
    mov     [arrayValor+ESI*2],AX 
    inc     ESI          
    inc     edi
    jmp arrayValores
    
finVariables:
    sub esi,esi         
    sub ebx,ebx
    call contador   
    ;PutLInt ecx
    sub ebx,ebx
    sub esi,esi
    jmp verLista        

verLista:
     
     mov ebx,[array+ESI*2]      ;se mueve el primer valor de la lista al registro ebx
     cmp bl,0h                  ;se compara con 0 para saber si ya se llego al final
     je fin
     PutCh bl
     inc ESI
     loop verLista
     
fin:

     .EXIT
    
     
     
